const path = require("path");
const http = require("http");
const express = require('express');
const socketio = require('socket.io');
const axios = require('axios');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const port = process.env.PORT || 3030;
const publicDirectoryPath = path.join(__dirname, '/public');

app.use(express.static(publicDirectoryPath));

io.on("connection", (client) => {
    console.log('New websocket connection');
    client.on('messageFromClient', msg => {
        console.log('receiving msg from client:', msg, 'clientID:', client.id);
        setTimeout(() => {
            axios.get(`https://www.omdbapi.com/?t=${msg}&apikey=abe9d6ee`)
                .then((d) => {
                    console.log(d.data.Plot);
                    const bb = msg + " ClientID: " + client.id + ' movie: ' + d.data.Plot;
                    io.emit('messageFromServer', bb);
                });
        }, 5000)


    });
    client.on('disconnect', () => {
        console.log('New websocket disconnected');
    });
})

server.listen(port, () => {
    console.log(`Server is up on port ${port}!`);
})